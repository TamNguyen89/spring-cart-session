package com.tam.controller;

import com.tam.model.Cart;
import com.tam.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

@Controller
@RequestMapping("/cart")
@SessionAttributes("cart")
public class CartController {

    @ModelAttribute("cart")
    public Cart setUpCart() {
        return new Cart();
    }

    @Autowired
    private ProductService productService;

    @GetMapping
    public String showCart() {
        return "cart";
    }

    @PostMapping("/addCart/{code}")
    public String addCart(@PathVariable String code,
                          @ModelAttribute("cart") Cart cart) {
        cart.addProduct(productService.getProductByCode(code));
        return "redirect:/products";
    }

    @GetMapping("/change")
    public String changeNumber(@RequestParam String code,
                               @RequestParam int number,
                               @ModelAttribute("cart") Cart cart) {
        cart.updateProductNumberByCode(code, number);
        return "cart";
    }

    @GetMapping("/{code}/delete")
    public String deleteProduct(@PathVariable String code,
                                @ModelAttribute("cart") Cart cart) {
        cart.deleteProductBycode(code);
        return "cart";
    }

    @PostMapping("/checkout")
    public String checkout(SessionStatus sessionStatus) {
        sessionStatus.setComplete();
        return "redirect:/products";
    }
}
