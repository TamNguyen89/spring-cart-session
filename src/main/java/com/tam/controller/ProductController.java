package com.tam.controller;

import com.tam.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping
    public String getProducts(Model model) {
        model.addAttribute("products", productService.getProducts());
        return "products";
    }

    @GetMapping("/{code}/view")
    public ModelAndView viewProduct(@PathVariable String code) {
        ModelAndView modelAndView = new ModelAndView("view");
        modelAndView.addObject("product", productService.getProductByCode(code));
        return modelAndView;
    }

}
