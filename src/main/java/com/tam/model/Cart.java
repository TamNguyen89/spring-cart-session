package com.tam.model;

import java.util.*;

public class Cart {

    private Map<String, Product> productMap = new HashMap<>();
    private long totalPrice;

    public Map<String, Product> getProductMap() {
        return productMap;
    }

    public void setProductMap(Map<String, Product> productMap) {
        this.productMap = productMap;
    }

    public long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getSize() {
        return productMap.values().size();
    }

    public List<Product> getProducts() {
        return new ArrayList<>(productMap.values());
    }

    public void addProduct(Product product) {
        Product productBuy = productMap.get(product.getCode());
        if (productBuy == null) {
            productBuy = new Product(product.getCode(), product.getName(), product.getPrice(), product.getImage());
            productBuy.increaseNumber();
        } else {
            productBuy.increaseNumber();
        }
        productMap.put(product.getCode(), productBuy);
    }

    public void updateProductNumberByCode(String code, int number) {
        this.getProducts().stream().forEach(product -> {
            if (product.getCode().equals(code)) {
                product.setNumber(number);
            }
        });
    }

    public void deleteProductBycode(String code) {
        productMap.remove(code);
    }
}
