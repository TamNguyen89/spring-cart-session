package com.tam.model;

public class Product {
    private String code;
    private String name;
    private double oldPrice;
    private double price;
    private String image;
    private int number;

    public Product(String code, String name, double oldPrice, double price, String image) {
        this.code = code;
        this.name = name;
        this.oldPrice = oldPrice;
        this.price = price;
        this.image = image;
    }

    public Product(String code, String name, double price, String image) {
        this.code = code;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(double oldPrice) {
        this.oldPrice = oldPrice;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void increaseNumber() {
        this.number+=1;
    }
}
