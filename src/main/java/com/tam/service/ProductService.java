package com.tam.service;

import com.tam.model.Product;

import java.util.List;

public interface ProductService {

    List<Product> getProducts();

    Product getProductByCode(String code);

}
