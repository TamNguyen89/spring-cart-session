package com.tam.service;

import com.tam.model.Product;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProductServiceImpl implements ProductService {

    Map<String, Product> products = new HashMap<>();

    {
        products.put("SP01", new Product("SP01", "Product 1", 100000, 80000, "product1"));
        products.put("SP02", new Product("SP02", "Product 2", 200000, 160000, "product2"));
        products.put("SP03", new Product("SP03", "Product 3", 300000, 250000, "product3"));
        products.put("SP04", new Product("SP04", "Product 4", 300000, 370000, "product4"));
    }


    @Override
    public List<Product> getProducts() {
        return new ArrayList<>(products.values());
    }

    @Override
    public Product getProductByCode(String code) {
        return products.get(code);
    }
}
